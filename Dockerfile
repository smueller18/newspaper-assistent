FROM python:3.7-alpine

WORKDIR /app
COPY Pipfile Pipfile.lock /app/

RUN apk add --no-cache --virtual .build-deps gcc musl-dev libffi-dev libressl-dev g++ && \
    pip install --no-cache-dir pipenv && \
    pipenv install -v --system && \
    # TODO remove
    pip install --no-cache-dir pytest pyyaml docker mongomock && \
    apk del --no-cache .build-deps

COPY app.py /app/
COPY newspaper_assistent /app/newspaper_assistent
# TODO remove
COPY tests /app/tests

VOLUME /app/data

EXPOSE 8080

CMD ["python", "/app/app.py"]

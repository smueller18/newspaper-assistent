#!/usr/bin/env python3
#
# Copyright 2018 Stephan Müller
#
# Licensed under the MIT license

import logging
import os

from newspaper_assistent.models import Article
from newspaper_assistent.webapp import WebApp
from tests.test_articles import insert_articles

LOGGING_LEVEL = os.getenv("LOGGING_LEVEL", "DEBUG")
logging_format = "%(asctime)s %(levelname)8s %(name)s %(message)s"
logging.basicConfig(level=logging.getLevelName(LOGGING_LEVEL), format=logging_format)

logger = logging.getLogger('app')

if __name__ == "__main__":

    if Article.objects.count() < 1:
        logger.info("Insert articles from tests/resources/bz-freiburg-2016-04-30.yml")
        insert_articles()

    webapp = WebApp()
    webapp.start()

# newspaper-assistent
[![pipeline status](https://gitlab.com/smueller18/newspaper-assistent/badges/master/pipeline.svg)](https://gitlab.com/smueller18/newspaper-assistent/commits/master)
[![documentation](https://smueller18.gitlab.io/newspaper-assistent/documentation.svg)](https://smueller18.gitlab.io/newspaper-assistent/)
[![coverage](https://gitlab.com/smueller18/newspaper-assistent/badges/master/coverage.svg)](https://smueller18.gitlab.io/newspaper-assistent/coverage/)
[![pylint](https://smueller18.gitlab.io/newspaper-assistent/lint/pylint.svg)](https://smueller18.gitlab.io/newspaper-assistent/lint/)

## Prerequisites

```
git config --global --add core.hooksPath githooks
```

#
# Copyright 2018 Stephan Müller
#
# Licensed under the MIT license

"""Utils for simplification"""

import re

from google.auth.transport import requests
from google.oauth2 import id_token

from newspaper_assistent import GOOGLE_CLIENT_ID


def trim(text: str) -> str:
    """
    Remove all tabs, newline characters and surrounding space characters
    :param text: text to trim
    :return: trimmed text or unmodified input parameter if not a string
    """
    if not isinstance(text, str):
        return text
    return re.sub(r"[\n\t]*", "", text).strip()


class InvalidTokenError(Exception):
    """Exception is thrown if decoding or verifying token failed."""
    pass


def google_oath2_decode(token) -> dict:
    """Decode google oath2 token with validation.

    :return: User id info

    :raise InvalidTokenError: If decoding of token failed.
    """
    try:
        # Specify the CLIENT_ID of the app that accesses the backend:
        idinfo = id_token.verify_oauth2_token(token, requests.Request(), GOOGLE_CLIENT_ID)

        if idinfo['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
            raise ValueError('Wrong issuer.')

        return idinfo

    except (ValueError, Exception):
        raise InvalidTokenError("Decoding token failed.")


def time_duration_to_ssml(duration: int):
    """Translates a time duration to SSML language.

    :param duration: duration in seconds
    :return: time duration in SSML format
    """
    minutes = duration // 60
    seconds = duration - minutes * 60

    ssml = ""
    if minutes > 0:
        ssml += f'<say-as interpret-as="unit">{minutes} minute</say-as>'

    if seconds > 0:
        if minutes > 0:
            ssml += ' und '
        ssml += f'<say-as interpret-as="unit">{seconds} second</say-as>'

    return ssml

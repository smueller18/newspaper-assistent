#
# Copyright 2018 Stephan Müller
#
# Licensed under the MIT license

import logging

import tornado.web

from newspaper_assistent.handlers import HealthHandler, PrivacyPolicyHandler, NewspaperAssistentHandler, \
    TermsOfServiceHandler

logger = logging.getLogger(__name__)


class WebApp:

    def __init__(self, project_id="newspaper-assistent", port=8080, autoreload=False):
        self._app = tornado.web.Application([
            ("/" + project_id, NewspaperAssistentHandler),
            ("/health", HealthHandler),
            ('/privacy-policy', PrivacyPolicyHandler),
            ('/terms-of-service', TermsOfServiceHandler),
        ], autoreload=autoreload)
        self.app.listen(port)

    @property
    def app(self) -> tornado.web.Application:
        return self._app

    @staticmethod
    def start():
        logger.info("Starting tornado")
        tornado.ioloop.IOLoop.current().start()

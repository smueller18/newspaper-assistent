#
# Copyright 2018 Stephan Müller
#
# Licensed under the MIT license

"""Module for providing tornado request handlers."""
import json
import logging
import os

import tornado.web
from bson import json_util
from mongoengine import connection

import newspaper_assistent
from newspaper_assistent import DEVELOPER_MODE
from newspaper_assistent.articles import count_sections, get_unread_articles, list_sections_ssml, set_article_state
from newspaper_assistent.dialogflow import Context, FollowupEventInput, WebhookRequest, WebhookResponse
from newspaper_assistent.google_assistent import Conversation
from newspaper_assistent.models import Article, ArticleStateName, User

logger = logging.getLogger(__name__)


class SecureRequestHandler(tornado.web.RequestHandler):
    def data_received(self, chunk):
        """Implement this method to handle streamed request data."""
        raise NotImplementedError

    def set_default_headers(self):
        self.set_header("Server", "test")


class HealthHandler(SecureRequestHandler):
    """Tornado web server handler for health checks"""

    def data_received(self, chunk):
        """Implement this method to handle streamed request data."""
        raise NotImplementedError()

    def get(self, *args, **kwargs):
        """Get method.

        :return: Status 200 "OK" if health check succeeds, Status 500 otherwise
        """
        try:
            if connection.get_connection()[newspaper_assistent.MONGODB_DATABASE].command('ping') == {'ok': 1}:
                self.write(b'OK')
            else:
                raise Exception()
        except Exception as e:
            self.send_error()
            logging.exception(e)


class PrivacyPolicyHandler(SecureRequestHandler):
    """Handler for serving privacy policy."""

    def data_received(self, chunk):
        pass

    def __init__(self, application, request, **kwargs):
        super().__init__(application, request, **kwargs)
        with open(os.path.join(os.path.dirname(__file__), 'resources', 'privacy-policy.html')) as f:
            self._privacy_policy_content = f.read()

    @tornado.gen.coroutine
    def get(self):
        self.write(self._privacy_policy_content)


class TermsOfServiceHandler(SecureRequestHandler):
    """Handler for serving terms of service."""

    def data_received(self, chunk):
        pass

    def __init__(self, application, request, **kwargs):
        super().__init__(application, request, **kwargs)
        with open(os.path.join(os.path.dirname(__file__), 'resources', 'terms-of-service.html')) as f:
            self._terms_of_service_content = f.read()

    @tornado.gen.coroutine
    def get(self):
        self.write(self._terms_of_service_content)


class NewspaperAssistentHandler(SecureRequestHandler):
    """Tornado web server handler for health checks"""

    intent_handlers = dict()

    def data_received(self, chunk):
        """Implement this method to handle streamed request data."""
        raise NotImplementedError()

    def write_response(self, response: dict):
        self.set_header("Content-Type", 'application/json')
        self.set_header('Google-Assistant-API-Version', 'v2')
        response_json = json.dumps(response, indent=2, default=json_util.default)
        self.write(response_json.encode())
        # TODO: remove logger
        logger.info(f"## Response ##\n {response_json}")

    def get(self, *args, **kwargs):
        """Get method."""
        self.write(b'ok')

    def post(self, *args, **kwargs):
        webhook_request = WebhookRequest.from_http_request(self.request)

        # TODO remove
        logger.info("## Request ##\n" + json.dumps(webhook_request, indent=2, default=json_util.default))

        self.write_response(WebhookRequestHandler(webhook_request).handle_post())


class WebhookRequestHandler:

    def __init__(self, webhook_request: WebhookRequest):
        self.webhook_request = webhook_request
        self.user = None

    def handle_post(self) -> WebhookResponse:

        if not DEVELOPER_MODE and not self.webhook_request.logged_in:
            return WebhookResponse(payload_google=Conversation.sign_in())

        if not DEVELOPER_MODE:
            google_id = self.webhook_request.user_info()['sub']
            name = self.webhook_request.user_info()['name']
            email = self.webhook_request.user_info()['email']
        else:
            google_id = "0"
            name = ""
            email = "mail@example.com"
        if User.objects(google_id=google_id).count() == 0:
            self.user = User(
                google_id=google_id,
                name=name,
                email=email
            ).save()
        else:
            self.user = User.objects(google_id=google_id).first()

        intent = self.webhook_request.intent
        if intent == "read_article_headline":
            return self.read_article_headline()

        if intent == "read_article_headline-fallback":
            return self.read_article_headline_fallback()

        if intent == "read_article_full":
            return self.read_article_full()

        if intent == "read_article_headline-decline":
            return WebhookResponse(
                fulfillment_text='Artikel wird überpsrungen.',
                followup_event_input=FollowupEventInput("event_read_article_headline_declined", "de-DE")
            )

        if intent == "list_sections":
            return WebhookResponse(
                fulfillment_text=(
                    f'Es gibt insgesamt {count_sections()} Kategorien. Soll ich sie aufsagen?'
                )
            )

        if intent == "list_sections-yes":
            return WebhookResponse(
                payload_google=Conversation.text(
                    ssml=f'Die Kategorien lauten: {list_sections_ssml()}'
                )
            )

        if intent == "count_unread_articles":

            text = f'Du hast {get_unread_articles(self.user).count()} ungelesene Artikel'
            section = self.user.settings.section
            if section is not None:
                text += (
                    f' in der gewählten Kategorie "{section}"'
                    f' und {get_unread_articles(self.user, ignore_user_section=True).count()}'
                )
            text += " für den gesamten Tag."

            return WebhookResponse(
                fulfillment_text=text
            )

        if intent == "choose_section":
            section = self.webhook_request.get_parameter("section")
            self.user.update_section(section)
            return WebhookResponse(
                fulfillment_text=f'Es werden nun nur noch Artikel der Kategorie "{section}" vorgelesen.'
            )

        if intent == "current_section":
            if self.user.settings.section is None:
                return WebhookResponse(
                    fulfillment_text=f'Es ist keine Kategorie gewählt.'
                )

            return WebhookResponse(
                fulfillment_text=f'Die derzeit gewählte Kategorie ist "{self.user.settings.section}".'
            )

        if intent == "reset_section":
            self.user.delete_section()
            return WebhookResponse(
                fulfillment_text=f'Die Kategorie wurde zurückgesetzt. Es werden nun wieder alle Artikel vorgelesen.'
            )

        if intent == "prioritize_section":
            section = self.webhook_request.get_parameter("section")
            priority = int(self.webhook_request.get_parameter("priority"))

            if not section or not priority:
                return WebhookResponse(
                    fulfillment_text=f'Ich kann keine Priorität setzen, wenn mir Informationen fehlen.'
                )

            priority_changed = False
            if priority < 0:
                priority = 0
                priority_changed = True
            elif priority > 5:
                priority = 5
                priority_changed = True

            text = ""
            if priority_changed:
                text += (
                    f'Du kannnst nur Prioritäten zwischen 0 und 5 vergeben. Die nächste Übereinstummung ist'
                    f' {priority}.'
                )
            text += f'Die Kategorie {section} wurde mit Priorität {priority} gespeichert.'

            self.user.update(**{'set__settings__section_preferences__' + section: priority})
            return WebhookResponse(
                fulfillment_text=text
            )

        if intent == "reset_section_preferences-yes":
            self.user.update(set__settings_section_preferences=dict())
            return WebhookResponse(
                fulfillment_text=f'Die Prioritäten wurden erfolgreich zurückgesetzt.'
            )

        if intent == "include_author-yes":
            if self.user.settings.include_author:
                return WebhookResponse(
                    fulfillment_text="Der Autor wird bereits nach jedem Titel erwähnt."
                )
            self.user.update(set__settings__include_author=True)
            return WebhookResponse(
                fulfillment_text=(
                    "Falls ein Autor für einen Artikel hinterlegt ist, wird dieser ab jetzt immer nach dem Titel"
                    " erwähnt."
                )
            )

        if intent == "include_author-no":
            if not self.user.settings.include_author:
                return WebhookResponse(
                    fulfillment_text="Das Vorlesen des Autors wurde bereits deaktiviert."
                )
            self.user.update(set__settings__include_author=False)
            return WebhookResponse(
                fulfillment_text=(
                    "Falls ein Autor für einen Artikel hinterlegt ist, wird dieser ab jetzt nicht mehr erwähnt."
                )
            )

        if intent == "reset_settings-yes":
            self.user.reset_settings()
            return WebhookResponse(
                fulfillment_text=(
                    "Die Benutzereinstellungen wurden zurückgesetzt."
                )
            )

        if intent == "delete_user-yes":
            self.user.delete()
            return WebhookResponse(
                fulfillment_text=(
                    "Die Benutzerdaten wurden vollständig gelöscht."
                )
            )

        if intent == "help" or intent == "default-fallback":
            ssml = (
                'Vorab solltest du wissen, immer wenn ich rede, kannst du mich mit <break time="0.5s"/>"OK '
                'Google"<break time="0.5s"/> '
                'unterbrechen.'
                ' Nachdem'
                ' du <break time="0.5s"/>"OK Google"<break time="0.5s"/> gesagt hast, musst du mir eine Anweisung'
                ' geben. Zum Beispiel könntest du mich'
                ' jetzt mit <break time="0.5s"/>"OK Google, Artikel vorlesen"<break time="0.5s"/> unterbrechen und ich'
                ' würde mit dem Vorlesen der Artikel beginnen.'
                ' Wie gerade schon erwähnt kannst du <break time="0.5s"/>"Artikel vorlesen"<break time="0.5s"/> sagen.'
                ' Zuerst sage ich dann die'
                ' Überschrift, gefolgt von dem Untertitel, dem Autor und der geschätzten Vorlesedauer auf.'
                ' Wenn der Artikel interessant ist, musst du das Vorlesen mit gängigen Bestätigungsausdrücken wie'
                ' <break time="0.5s"/>"OK"<break time="0.5s"/>, <break time="0.5s"/>"vorlesen"<break time="0.5s"/>'
                ' oder <break time="0.5s"/>"gut"<break time="0.5s"/> bestätigen. Willst du zum nächsten Artikel'
                ' springen,'
                ' sage gängige Verneinungsausdrücke wie <break time="0.5s"/>"Nein"<break time="0.5s"/>,'
                ' <break time="0.5s"/>"Auf keinen Fall"<break time="0.5s"/> oder'
                ' <break time="0.5s"/>"interessiert mich nicht"<break time="0.5s"/>.'
                ' Du kannst auch zwischen'
                ' verschiedenen Kategorien wählen. Wenn du erfahren willst, welche Kategorien zu Verfügung stehen,'
                ' frage <break time="0.5s"/>"Welche Kategorien gibt es"<break time="0.5s"/>. Um zum Beispiel die'
                ' Kategorie "Sport" zu wählen, sage <break time="0.5s"/>"Wähle'
                ' Kategorie Sport"<break time="0.5s"/>. Wenn du wieder Artikel aus allen Kategorien vorgelesen bekommen'
                ' möchtest, setze'
                ' die Kategorieauswahl mit <break time="0.5s"/>"Kategorie zurücksetzen"<break time="0.5s"/> zurück.'
                ' Normalerweise werden Autoren von'
                ' Artikeln genannt, wenn dir der Name des Autors egal ist, sage '
                '<break time="0.5s"/>"Kein Autor mehr"<break time="0.5s"/>. Wenn du das'
                ' rückgängig machen willst, sage <break time="0.5s"/>"Autor vorlesen"<break time="0.5s"/>.'
            )
            return WebhookResponse(
                payload_google=Conversation.text(
                    ssml=ssml
                )
            )

        if intent == "close":
            return self.close()

        return self.close("Für diese Anfrage habe ich leider keine Antwort.")

    def read_article_headline(self) -> WebhookResponse:
        article = get_unread_articles(self.user).first()

        if article is None:
            return WebhookResponse(
                payload_google=Conversation.text(
                    ssml=f'Ich kann keine ungelesenen Artikel mehr finden. Probiere es mit einer anderen Kategorie.'
                )
            )

        set_article_state(self.user, article, ArticleStateName.TITLE_READING)

        # TODO beautify 2 responses
        ssml = article.headline(self.user)

        if self.webhook_request.has_context("event_read_article_headline_declined"):
            ssml = 'Dann lese ich den nächsten Artikel vor.<break time="2s"/>' + ssml

        return WebhookResponse(
            payload_google=Conversation.text(
                ssml=ssml
            ),
            output_contexts=[
                Context(self.webhook_request.session, "article", 3, {'articleId': str(article.id)}),
                Context(self.webhook_request.session, "read_article_full", 1)
            ],
            # followup_event_input=FollowupEventInput("full_article", "de-DE")
        )

    def read_article_full(self) -> WebhookResponse:
        article_id = self.webhook_request.get_context("article")['parameters']['articleId']

        article = Article.objects(id=article_id).first()
        set_article_state(self.user, article, ArticleStateName.FULL_READING)

        return WebhookResponse(
            payload_google=Conversation.text(
                ssml=article.text
            ),
            output_contexts=[]
        )

    def close(self, text: str = "Auf Wiedersehen.") -> WebhookResponse:
        return WebhookResponse(payload_google=Conversation.close(text))

    def read_article_headline_fallback(self):
        article_id = self.webhook_request.get_context("article")['parameters']['articleId']

        contexts = list()
        Context(self.webhook_request.session, "article", 3, {'articleId': str(article_id)}),
        Context(self.webhook_request.session, "read_article_headline-fallback", 1),
        if self.webhook_request.has_context("read_article_intro"):
            contexts.append(Context(self.webhook_request.session, "read_article_intro", 1))
        elif self.webhook_request.has_context("read_article_full"):
            contexts.append(Context(self.webhook_request.session, "read_article_full", 1))

        return WebhookResponse(
            fulfillment_text='Sage "Ja" um den Artikel vorlesen zu lassen oder "Nein", um den nächsten Artikel '
                             'vorlesen zu lassen.',
            output_contexts=contexts
        )

    def read_article_intro_fallback(self):
        article_id = self.webhook_request.get_context("article")['parameters']['articleId']

        contexts = list()
        Context(self.webhook_request.session, "article", 3, {'articleId': str(article_id)}),
        Context(self.webhook_request.session, "read_article_intro-fallback", 1),
        contexts.append(Context(self.webhook_request.session, "read_article_full", 1))

        return WebhookResponse(
            fulfillment_text='Sage "Ja" um den Hauptteil des Artikels vorlesen zu lassen oder "Nein", um zum nächsten'
                             ' Artikel überzugehen.',
            output_contexts=contexts
        )

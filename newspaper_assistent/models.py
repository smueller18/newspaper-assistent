#
# Copyright 2018 Stephan Müller
#
# Licensed under the MIT license

"""Models for mongodb."""

from datetime import datetime, date
from enum import Enum

from mongoengine import (DateField, DateTimeField, Document, EmailField, IntField, LazyReferenceField,
                         StringField, EmbeddedDocument, ListField, EmbeddedDocumentField, DictField, BooleanField,
                         CASCADE)

from newspaper_assistent.utils import time_duration_to_ssml


class UserSettings(EmbeddedDocument):
    version = IntField(required=True, default=1)
    section = StringField()
    date = DateField(default=date(2016, 4, 30))
    section_preferences = DictField(default=dict())
    include_author = BooleanField(default=True)
    include_duration_prediction = BooleanField(default=True)


class User(Document):
    version = IntField(required=True, default=1)
    google_id = StringField(required=True, unique=True)
    name = StringField(required=True)
    email = EmailField(required=True)
    settings = EmbeddedDocumentField(UserSettings, default=UserSettings)

    def update_section(self, section):
        self.update(set__settings__section=section)

    def delete_section(self):
        self.update(set__settings__section=None)

    def reset_settings(self):
        self.update(set__settings=UserSettings())


class Article(Document):
    """Newspaper article."""
    version = IntField(required=True, default=1)
    newspaper: str = StringField(required=True)
    locale: str = StringField(required=False)
    section: str = StringField(required=True)
    location: str = StringField()
    title: str = StringField(required=True)
    subtitle: str = StringField()
    intro_text: str = StringField()
    full_text: str = StringField(required=True)
    author: str = StringField()
    date: datetime = DateField(required=True)
    duration = IntField(default=0)
    meta = {
        'auto_create_index': False,
    }

    def has_intro(self) -> bool:
        return self.intro_text is not None

    def headline(self, user: User = None) -> str:
        """Summary of article.

        :return: SSML formatted headline with location, title, subtitle and author.
        """

        output = ""
        if self.location is not None:
            output += self.location + '.<break time="750ms"/>'

        output += self.title + ". "

        if self.subtitle is not None:
            output += f'<break time="250ms"/>{self.subtitle}.'

        if self.author is not None and (user is None or (user is not None and user.settings.include_author)):
            output += f'<break time="750ms"/>Geschrieben von {self.author}.'

        if self.duration > 0 and (user is None or (user is not None and user.settings.include_duration_prediction)):
            output += (
                f'<break time="1s"/>Die geschätzte Vorlesedauer beträgt etwa'
                f' {time_duration_to_ssml(self.duration)}.'
            )

        return output

    @property
    def text(self):
        if self.intro_text is not None:
            return f'{self.intro_text}<break time="1s"/>{self.full_text}'
        return self.full_text


class ArticleStateName(Enum):
    """State of Articles."""
    TITLE_READING = 1
    TITLE_READ = 2
    TITLE_ABORTED = 3
    INTRO_READING = 4
    INTRO_ABORTED = 5
    FULL_READING = 6
    FULL_READ = 7
    FULL_ABORTED = 8


class ArticleState(EmbeddedDocument):
    version = IntField(required=True, default=1)
    state = IntField(required=True, choices=[e.value for e in ArticleStateName])
    timestamp = DateTimeField(required=True, default=datetime.now)


class UserArticleLifecycle(Document):
    version = IntField(required=True, default=1)
    user = LazyReferenceField(User, reverse_delete_rule=CASCADE)
    article = LazyReferenceField(Article, reverse_delete_rule=CASCADE)
    states = ListField(EmbeddedDocumentField(ArticleState))
    meta = {
        'allow_inheritance': True
    }

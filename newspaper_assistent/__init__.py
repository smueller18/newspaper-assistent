#
# Copyright 2018 Stephan Müller
#
# Licensed under the MIT license

"""Initialize module parameter from environment settings."""
import tornado.log
from mongoengine import DEFAULT_CONNECTION_NAME, connection, register_connection
from smart_getenv import getenv

tornado.log.enable_pretty_logging()

GOOGLE_CLIENT_ID = getenv("GOOGLE_CLIENT_ID", type=str)

PROJECT_ID = getenv("PROJECT_ID", default="newspaper-assistent", type=str)
if PROJECT_ID is None or not isinstance(PROJECT_ID, str):
    raise ValueError("Environment variable PROJECT_ID must be set.")

MONGODB_HOST = getenv("MONGODB_HOST", default="mongodb", type=str)
MONGODB_PORT = getenv("MONGODB_PORT", default=27017, type=int)
MONGODB_DATABASE = getenv("MONGODB_DATABASE", default="default", type=str)
MONGODB_CONNECT_TIMEOUT_MS = getenv("MONGODB_CONNECT_TIMEOUT_MS", default=5000, type=int)
MONGODB_SERVER_SELECTION_TIMEOUT_MS = getenv("MONGODB_SERVER_SELECTION_TIMEOUT_MS", default=5000, type=int)

DEVELOPER_MODE = getenv("DEVELOPER_MODE", default=False, type=bool)


def initialize(reconnect: bool = False):
    """Initialize mongodb connection.

    :param reconnect: if ``True``, disconnect mongodb connection
    """
    if reconnect:
        connection.disconnect(DEFAULT_CONNECTION_NAME)
    register_connection(
        DEFAULT_CONNECTION_NAME,
        host=MONGODB_HOST,
        port=MONGODB_PORT,
        db=MONGODB_DATABASE,
        connectTimeoutMS=MONGODB_CONNECT_TIMEOUT_MS,
        serverSelectionTimeoutMS=MONGODB_SERVER_SELECTION_TIMEOUT_MS
    )


initialize()

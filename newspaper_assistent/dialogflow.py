#
# Copyright 2018 Stephan Müller
#
# Licensed under the MIT license

from __future__ import annotations

import json
import logging
import os
from typing import List, Union

import tornado.httpserver
import tornado.web

from newspaper_assistent.dictionary import SerializableDict
from newspaper_assistent.google_assistent import Conversation
from newspaper_assistent.utils import InvalidTokenError, google_oath2_decode

__dirname__ = os.path.abspath(os.path.dirname(__file__))
logger = logging.getLogger(__name__)


class NotLoggedInError(Exception):
    """Exception is thrown if user is not logged in."""
    pass


class Context(SerializableDict):
    """Context object of dialogflow API"""

    def __init__(self, session: str, id: str, lifespan_count: int = 0, parameters: dict = None):
        """Initializes a new instance

        :param session: first part of the context name
        :param id: last part of the context name
        :param lifespan_count: The number of conversational query requests after which the context expires. If set to 0
            (the default) the context expires immediately. Contexts expire automatically after 20 minutes even if there
            are no matching queries.
        :param parameters: The collection of parameters associated with this context.
        """
        context = {
            'name': session + "/contexts/" + id,
            'lifespanCount': lifespan_count
        }
        if parameters is not None:
            context.update({'parameters': parameters})
        super().__init__(context)


class FollowupEventInput(dict):
    def __init__(self, name: str, language_code: str, parameters: dict = None):
        event_input = {
            "name": name,
            "languageCode": language_code
        }
        if parameters is not None:
            event_input.update({'parameters': parameters})
        super().__init__(event_input)


class WebhookResponse(dict):
    """Response of a dialog webhook request."""

    def __init__(self, fulfillment_text: str = None, fulfillment_message: dict = None,
                 source: str = None, payload_google: Conversation = None, output_contexts: List[Context] = None,
                 followup_event_input: dict = None):

        response = dict()
        if followup_event_input is not None:
            response.update({"followupEventInput": followup_event_input})

        if fulfillment_text is not None:
            response.update({'fulfillmentText': fulfillment_text})

        if fulfillment_message is not None:
            response.update({'fulfillmentMessage': fulfillment_message})

        if source is not None:
            response.update({'source': source})

        if payload_google is not None:
            response.update({
                'payload': {
                    'google': payload_google
                }
            })

        if output_contexts is not None:
            response.update({'outputContexts': output_contexts})

        super().__init__(response)


class WebhookRequest(dict):
    """Request of a dialogflow webhook."""

    @classmethod
    def from_http_request(cls, request: tornado.httpserver.HTTPRequest):
        return WebhookRequest(json.loads(request.body.decode('utf-8')))

    @property
    def intent(self) -> str:
        """Display name of the intent."""
        return self['queryResult']['intent']['displayName']

    @property
    def output_contexts(self) -> str:
        """Display name of the intent."""
        return self['queryResult']['outputContexts']

    @property
    def session(self) -> str:
        """Session id."""
        return self['session']

    def user_info(self) -> dict:
        """Get user info from request.

        :return: User info
        """
        try:
            return google_oath2_decode(self['originalDetectIntentRequest']['payload']['user']['idToken'])
        except KeyError:
            raise NotLoggedInError("Request does not belong to a logged in user.")
        except InvalidTokenError as e:
            raise e

    @property
    def logged_in(self) -> bool:
        """Checks if request was sent by a logged in user.

        :return: ``True`` if user is logged in, ``False`` otherwise.
        """
        try:
            return isinstance(self.user_info(), dict)
        except NotLoggedInError:
            return False
        except InvalidTokenError as e:
            raise e

    def has_context(self, context_id: str) -> bool:
        try:
            for context in self['queryResult']['outputContexts']:
                if context['name'] == self.session + "/contexts/" + context_id:
                    return True
            return False
        except (KeyError, TypeError):
            return False

    def get_context(self, context_id: str):
        try:
            for context in self['queryResult']['outputContexts']:
                if context['name'] == self.session + "/contexts/" + context_id:
                    return context
        except (KeyError, TypeError):
            return False
        return False

    def has_parameter(self, name: str) -> bool:
        try:
            if name in self['queryResult']['parameters']:
                return True
            return False
        except (KeyError, TypeError):
            return False

    def get_parameter(self, name: str) -> Union[bool, str, dict]:
        try:
            return self['queryResult']['parameters'][name]
        except (KeyError, TypeError):
            return False

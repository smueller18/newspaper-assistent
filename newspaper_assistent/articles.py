#
# Copyright 2018 Stephan Müller
#
# Licensed under the MIT license

"""Interfaces for articles"""

import datetime

from mongoengine.queryset import QuerySet

from newspaper_assistent.models import Article, ArticleStateName, UserArticleLifecycle, User, ArticleState


def get_unread_articles(user: User, ignore_user_section=False) -> QuerySet:
    filter = dict()

    read_articles = UserArticleLifecycle.objects(states__exists=True, user=user.id).distinct('article')
    filter.update({'id__nin': read_articles})
    if not ignore_user_section and user.settings.section is not None:
        filter.update({'section': user.settings.section})
    if user.settings.date is not None:
        filter.update({'date__gte': user.settings.date})
        filter.update({'date__lt': user.settings.date + datetime.timedelta(days=1)})

    return Article.objects(**filter)


def set_article_state(user: User, article: Article, state: ArticleStateName):
    article_state = ArticleState(state=state.value)
    if UserArticleLifecycle.objects(user=user, article=article).count() > 0:
        UserArticleLifecycle.objects(user=user, article=article).first().update(add_to_set__states=article_state)
    else:
        UserArticleLifecycle(user=user, article=article, states=[article_state]).save()


def count_sections():
    """Determine number of all available sections

    :return: number of available sections
    """
    return len(Article.objects().distinct('section'))


def list_sections() -> list:
    """List of all available sections

    :return: sorted list with sections
    """
    return sorted(list(Article.objects().distinct('section')))


def list_sections_ssml() -> str:
    """List of all available sections in SSML format with break

    :return: sorted list with sections
    """
    return ',<break time="1s"/>'.join(list_sections())

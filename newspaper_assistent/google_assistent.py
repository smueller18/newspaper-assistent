from __future__ import annotations

from typing import List

from newspaper_assistent.dictionary import SerializableDict


class Conversation(SerializableDict):
    interpret_as_characters = [
        "AfD",
        "OB"
    ]

    replace_words = [
        (" & ", " und ")
    ]

    def __init__(self, *arg, **kwargs):
        super().__init__(*arg, **kwargs)

    @classmethod
    def text(cls, ssml: str = None, text_to_speech: str = None) -> Conversation:
        return Conversation({
            'expectUserResponse': True,
            'richResponse': {
                "items": [cls._simple_response(ssml, text_to_speech)],
            }
        })

    @classmethod
    def close(cls, ssml: str = None, text_to_speech: str = None) -> Conversation:
        return Conversation({
            'finalResponse': {
                'richResponse': {
                    "items": [cls._simple_response(ssml, text_to_speech)],
                }
            }
        })

    @classmethod
    def ask(cls, ssml: str = None, text_to_speech: str = None) -> Conversation:
        return Conversation({
            'expectUserResponse': True,
            'expectedInputs': [{
                'possibleIntents': {'intent': 'actions.intent.TEXT'},
                'inputPrompt': cls._input_prompt_simple_response(ssml, text_to_speech)
            }]
        })

    @classmethod
    def confirmation(cls, text: str, suggestions: dict = None) -> Conversation:
        """

        :param text: Request confirmation text
        :param suggestions:
        """
        if text is None:
            raise AttributeError("text is not set")

        if suggestions is None:
            return Conversation({
                'expectUserResponse': True,
                'expectedInputs': [{
                    'possibleIntents': {
                        'intent': 'actions.intent.CONFIRMATION',
                        'inputValueData': {
                            '@type': 'type.googleapis.com/google.actions.v2.ConfirmationValueSpec',
                            'dialogSpec': {
                                'requestConfirmationText': f"{text}"
                            }
                        }
                    }
                }]
            })

        return Conversation({
            'expectUserResponse': True,
            'expectedInputs': [{
                'possibleIntents': {
                    'intent': 'actions.intent.CONFIRMATION',
                    'inputValueData': {
                        '@type': 'type.googleapis.com/google.actions.v2.ConfirmationValueSpec',
                        'dialogSpec': {
                            'requestConfirmationText': f"{text}"
                        }
                    }
                },
                'inputPrompt': {
                    'richInitialPrompt': {
                        'suggestions': [suggestions]
                    }
                }
            }]
        })

    @classmethod
    def option(cls, ssml: str = None, text_to_speech: str = None, options: List[dict] = None) -> Conversation:
        """List[
            {
              'key': string,
              'synonyms': List[str]
            }
        ]
        """

        if options is None or not isinstance(options, list):
            raise AttributeError("options must be of type List[dict]")

        items = list()
        for option in options:
            if 'key' not in option:
                raise AttributeError(f"the option {option} does not contain the key 'key'")
            if 'synonyms' not in option:
                raise AttributeError(f"the option {option} does not contain the key 'synonyms'")

            items.append({'optionInfo': option, 'title': option['key']})

        return Conversation({
            'expectUserResponse': True,
            'expectedInputs': [{
                'inputPrompt': cls._input_prompt_simple_response(ssml, text_to_speech),
                'possibleIntents': [
                    {
                        'intent': 'actions.intent.OPTION',
                        'inputValueData': {
                            '@type': 'type.googleapis.com/google.actions.v2.OptionValueSpec',
                            'simpleSelect': {
                                'items': items
                            }
                        }
                    }
                ]
            }]
        })

    @classmethod
    def sign_in(cls):
        return Conversation({
            "expectUserResponse": True,
            "systemIntent": {
                "intent": "actions.intent.SIGN_IN",
                "data": {}
            }
        })

    @classmethod
    def _input_prompt_simple_response(cls, ssml: str = None, text_to_speech: str = None) -> dict:
        return {
            'richInitialPrompt': {
                'items': [cls._simple_response(ssml, text_to_speech)]
            }
        }

    @classmethod
    def _simple_response(cls, ssml: str = None, text_to_speech: str = None) -> dict:
        response = dict()

        if ssml is None and text_to_speech is None:
            raise AttributeError("Either ssml or text_to_speech must be defined.")

        elif ssml is not None:
            response.update({'ssml': f'<speak>{cls.improve_ssml(ssml)}</speak>'})

        elif text_to_speech is not None:
            response.update({'textToSpeech': text_to_speech})

        return {
            'simpleResponse': response
        }

    @classmethod
    def improve_ssml(cls, ssml: str):
        """Improve texts by adding SSML elements around defined words.

        :param ssml: Text to improve
        :return: Improved text
        """
        for word in cls.interpret_as_characters:
            ssml = ssml.replace(word, f'<say-as interpret-as="characters">{word}</say-as>')

        for search, replace in cls.replace_words:
            ssml = ssml.replace(search, replace)

        return ssml

#
# Copyright 2018 Stephan Müller
#
# Licensed under the MIT license

import json


class SerializableDict(dict):
    """Classes that can be serialized in JSON format."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def serialize(self) -> str:
        """Serialize JSON

        :return: Serialized JSON object string
        """
        return json.dumps(self)

    def encode(self) -> bytes:
        """Encode serialized JSON object

        :return: Encoded serialized JSON object
        """
        return self.serialize().encode()

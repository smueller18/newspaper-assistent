#!/usr/bin/python3
#
# Copyright 2018 Stephan Müller
#
# Licensed under the MIT license

import os
import sys

sys.path.insert(0, os.path.abspath('..'))

extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.coverage',
    'sphinx.ext.intersphinx',
]

templates_path = ['_templates']
exclude_patterns = ['_build']
# html_static_path = ['_static']

source_suffix = '.rst'
master_doc = 'index'

html_theme = 'sphinx_rtd_theme'
pygments_style = 'sphinx'
htmlhelp_basename = 'newspaper-assistent'

autodoc_default_flags = ['special-members', 'private-members', 'show-inheritance']

# objects.inv
intersphinx_mapping = {
    'python': ('https://docs.python.org/3', None),
    'jsonschema': ('https://python-jsonschema.readthedocs.io/en/latest', None),
    'pymongo': ('https://api.mongodb.com/python/current', None),
    'mongoengine': ('https://mongoengine.readthedocs.io/en/latest', None),
    'tornado': ('https://tornado.readthedocs.io/en/latest', None),
}

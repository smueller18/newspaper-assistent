Newspaper-assistent Documentation
=================================

Contents:

.. toctree::
    :maxdepth: 2
    :caption: API Documentation
    :glob:

    api/*


******************
Indices and tables
******************

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

#
# Copyright 2018 Stephan Müller
#
# Licensed under the MIT license

import pytest

import newspaper_assistent
from newspaper_assistent.models import UserArticleLifecycle, Article, User


@pytest.mark.skip
def test():
    newspaper_assistent.MONGODB_HOST = "localhost"
    newspaper_assistent.MONGODB_PORT = 27017
    newspaper_assistent.initialize(reconnect=True)

    user_id = User.objects.first().id

    cursor = UserArticleLifecycle.objects.aggregate(*[
        {'$match': {'states': {'$exists': True, '$ne': []}}},
        {'$match': {'user': user_id}},
        {
            '$lookup': {
                'from': 'article',
                'localField': '_id',
                'foreignField': 'article',
                'as': 'relation'}
        }])

    articles_touched = UserArticleLifecycle.objects(
        states__exists=True,
        user=user_id
    ).distinct('article')

    articles_untouched = Article.objects(
        id__nin=articles_touched
    ).distinct('title')

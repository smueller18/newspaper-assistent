#
# Copyright 2018 Stephan Müller
#
# Licensed under the MIT license

import time

import docker
import tornado.testing

import newspaper_assistent


class MongoInstance:
    socket, port = tornado.testing.bind_unused_port()
    socket.close()

    def __init__(self):
        self.container = None
        self.client = docker.from_env()

    def run(self):
        self.container = self.client.containers.run(
            "mongo:latest",
            detach=True,
            ports={'27017/tcp': self.port},
            auto_remove=True
        )

        while "waiting for connections on port 27017" not in self.container.logs().decode():
            time.sleep(0.1)

        # "localhost" for local tests and "docker" for Gitlab CI tests
        newspaper_assistent.MONGODB_HOST = self.client.api.base_url.split("//")[1].split(":")[0]
        newspaper_assistent.MONGODB_PORT = self.port
        newspaper_assistent.MONGODB_CONNECT_TIMEOUT_MS = 1000
        newspaper_assistent.MONGODB_SERVER_SELECTION_TIMEOUT_MS = 1000
        newspaper_assistent.initialize(reconnect=True)

    def __del__(self):
        try:
            self.container.kill()
        except Exception as e:
            print(e)

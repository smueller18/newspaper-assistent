#!/usr/bin/python3
#
# Copyright 2018 Stephan Müller
#
# Licensed under the MIT license

import pytest

from newspaper_assistent.google_assistent import Conversation


class TestGoogleAssistent:

    def test_confirmation(self):
        expectation = {
            'expectUserResponse': True,
            'expectedInputs': [{
                'possibleIntents': {
                    'intent': 'actions.intent.CONFIRMATION',
                    "inputValueData": {
                        "@type": "type.googleapis.com/google.actions.v2.ConfirmationValueSpec",
                        "dialogSpec": {
                            "requestConfirmationText": f"text"
                        }
                    }
                }
            }]
        }
        actual = Conversation.confirmation("text")
        assert isinstance(actual, Conversation)
        assert expectation == actual

        suggestions = {
            'key1': 'value',
            'key2': 'value'
        }
        expectation = {
            'expectUserResponse': True,
            'expectedInputs': [{
                'possibleIntents': {
                    'intent': 'actions.intent.CONFIRMATION',
                    "inputValueData": {
                        "@type": "type.googleapis.com/google.actions.v2.ConfirmationValueSpec",
                        "dialogSpec": {
                            "requestConfirmationText": f"text"
                        }
                    }
                },
                'inputPrompt': {
                    'richInitialPrompt': {
                        'suggestions': [{
                            'key1': 'value',
                            'key2': 'value'
                        }]
                    }
                }
            }]
        }

        actual = Conversation.confirmation("text", suggestions)
        assert isinstance(actual, Conversation)
        assert expectation == actual

        with pytest.raises(AttributeError) as e:
            Conversation.confirmation(None)
        assert "text is not set" == str(e.value)

    def test_option(self):
        options = [{
            'key': 'key1',
            'synonyms': ['synonym1', 'synonym2']
        }, {
            'key': 'key2',
            'synonyms': ['synonym1', 'synonym2']
        }]

        expectation = {
            'expectUserResponse': True,
            "expectedInputs": [{
                'inputPrompt': {
                    'richInitialPrompt': {
                        'items': [{
                            'simpleResponse': {
                                'ssml': f'<speak>text</speak>'
                            }
                        }]
                    }
                },
                "possibleIntents": [
                    {
                        "intent": "actions.intent.OPTION",
                        "inputValueData": {
                            "@type": "type.googleapis.com/google.actions.v2.OptionValueSpec",
                            "simpleSelect": {
                                "items": [{
                                    'optionInfo': {
                                        'key': 'key1',
                                        'synonyms': ['synonym1', 'synonym2']
                                    },
                                    'title': 'key1'
                                }, {
                                    'optionInfo': {
                                        'key': 'key2',
                                        'synonyms': ['synonym1', 'synonym2']
                                    },
                                    'title': 'key2'
                                }]
                            }
                        }
                    }
                ]
            }]
        }

        actual = Conversation.option('text', options=options)
        assert isinstance(actual, Conversation)
        assert expectation == actual

        with pytest.raises(AttributeError) as e:
            Conversation.option('text', options=None)
        assert "options must be of type List[dict]" == str(e.value)

        with pytest.raises(AttributeError) as e:
            Conversation.option('text', options=False)
        assert "options must be of type List[dict]" == str(e.value)

        with pytest.raises(AttributeError) as e:
            Conversation.option('text', options=[{'test': 'test'}])
        assert "the option {'test': 'test'} does not contain the key 'key'" == str(e.value)

        with pytest.raises(AttributeError) as e:
            Conversation.option('text', options=[{'key': 'test'}])
        assert "the option {'key': 'test'} does not contain the key 'synonyms'" == str(e.value)

    def test_sign_in(self):
        expected = {
            "expectUserResponse": True,
            "systemIntent": {
                "intent": "actions.intent.SIGN_IN",
                "data": {}
            }
        }
        assert Conversation.sign_in() == expected

    def test_text(self):
        expected = {
            'expectUserResponse': True,
            'richResponse': {
                'items': [
                    {
                        'simpleResponse': {
                            'ssml': '<speak>text</speak>'
                        }
                    }
                ]
            }
        }

        actual = Conversation.text("text")
        assert isinstance(actual, Conversation)
        assert expected == actual

    def test_close(self):
        expected = {
            'finalResponse': {
                'richResponse': {
                    "items": [{
                        'simpleResponse': {
                            'ssml': '<speak>text</speak>'
                        }
                    }]
                }
            }
        }

        actual = Conversation.close("text")
        assert isinstance(actual, Conversation)
        assert expected == actual

    def test_simple_response(self):
        with pytest.raises(AttributeError) as e:
            Conversation._simple_response()
        assert "Either ssml or text_to_speech must be defined.", str(e.exception)

        expected = {
            'simpleResponse': {
                'ssml': '<speak>text</speak>'
            }
        }
        actual = Conversation._simple_response(ssml="text")
        assert not isinstance(actual, Conversation)
        assert expected == actual

        expected = {
            'simpleResponse': {
                'textToSpeech': 'text'
            }
        }
        actual = Conversation._simple_response(text_to_speech="text")
        assert not isinstance(actual, Conversation)
        assert expected == actual

    def test_ask(self):
        expected = {
            'expectUserResponse': True,
            'expectedInputs': [{
                'possibleIntents': {'intent': 'actions.intent.TEXT'},
                'inputPrompt': {
                    'richInitialPrompt': {
                        'items': [{
                            'simpleResponse': {
                                'ssml': '<speak>text</speak>'
                            }
                        }]
                    }
                }
            }]
        }
        actual = Conversation.ask("text")
        assert isinstance(actual, Conversation)
        assert expected == actual

    def test_improve_ssml(self):
        assert Conversation.improve_ssml("AfD") == '<say-as interpret-as="characters">AfD</say-as>'
        assert Conversation.improve_ssml(" & ") == ' und '
        assert Conversation.improve_ssml("&") == '&'

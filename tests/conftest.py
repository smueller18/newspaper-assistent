import pytest

from tests.mongo import MongoInstance


@pytest.fixture()
def use_mongo_instance():
    mongo_instance = MongoInstance()
    mongo_instance.run()

    yield

    del mongo_instance

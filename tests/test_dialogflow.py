#
# Copyright 2018 Stephan Müller
#
# Licensed under the MIT license

import pytest

from newspaper_assistent.google_assistent import Conversation
from newspaper_assistent.dialogflow import WebhookResponse, WebhookRequest

"""
import dialogflow_v2 as df

def test_d():

    os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = os.path.abspath(os.path.join(__dirname__, "..", "credentials.json"))

    project_id = "mueller-c6e57"
    display_name = "Tiere"
    kind = ""

    client = df.EntityTypesClient()

    parent = client.project_agent_path(project_id)

    for element in client.list_entity_types(parent):
        print(element)


    entity_type = df.types.EntityType(display_name=display_name, kind="KIND_MAP")

    response = client.update_entity_type(entity_type)

    print('Entity type created: \n{}'.format(response))
"""


class TestWebhookResponse:

    @pytest.mark.parametrize("conversation", [
        Conversation.ask("Hello"),
        Conversation.close("Good bye"),
        Conversation.option("Choose", options=[{'key': 'key', 'synonyms': ['syn']}]),
        Conversation.confirmation("Hello")
    ])
    def test_from_conversation(self, conversation):
        response = WebhookResponse(payload_google=conversation)
        expected = {
            'payload': {
                'google': conversation
            }
        }
        assert isinstance(response, WebhookResponse)
        assert expected == response


class TestWebhookRequest:

    def test_get_intent(self):
        request = WebhookRequest({
            "queryResult": {
                "intent": {
                    "displayName": "read_article"
                },
            },
        })

        assert request.intent == "read_article"

    def test_context(self):
        request_empty = WebhookRequest({})

        request_empty_context = WebhookRequest({
            "session": "session",
            "queryResult": {
                "outputContexts": None
            }
        })

        request = WebhookRequest({
            "session": "session",
            "queryResult": {
                "outputContexts": [{
                    "name": "session/contexts/context1"
                }, {
                    "name": "session/contexts/context2"
                }]
            }
        })

        assert request_empty.has_context("context") is False
        assert request_empty_context.has_context("context") is False

        assert request.has_context("context1")
        assert request.has_context("context2")
        assert request.has_context("context3") is False

        assert request.get_context("context1") == {"name": "session/contexts/context1"}
        assert request.get_context("context2") == {"name": "session/contexts/context2"}

        assert request_empty.get_context("context") is False
        assert request_empty_context.get_context("context") is False
        assert request.get_context("context3") is False

    def test_parameter(self):
        request_empty = WebhookRequest({})

        request_empty_parameters = WebhookRequest({
            "queryResult": {
                "parameters": None
            }
        })

        request = WebhookRequest({
            "queryResult": {
                "parameters": {
                    "parameter1": "value"
                }
            }
        })

        assert request_empty.has_parameter("parameter1") is False
        assert request_empty_parameters.has_parameter("parameter1") is False
        assert request.has_parameter("parameter1")

        assert request_empty.get_parameter("parameter1") is False
        assert request_empty_parameters.get_parameter("parameter1") is False
        assert request.get_parameter("parameter1") == "value"

#
# Copyright 2018 Stephan Müller
#
# Licensed under the MIT license

import platform

import pytest
from smart_getenv import getenv

requires_linux = pytest.mark.skipif(
    platform.system() != "Linux",
    reason="Requires Linux"
)

integration_test = pytest.mark.skipif(
    getenv("SKIP_INTEGRATION_TESTS", type=bool, default=False),
    reason="Skip integration tests"
)

#
# Copyright 2018 Stephan Müller
#
# Licensed under the MIT license

import pytest
from mongoengine import DEFAULT_CONNECTION_NAME
from tornado.simple_httpclient import HTTPTimeoutError

import newspaper_assistent
from newspaper_assistent.webapp import WebApp


@pytest.fixture
def app():
    webapp = WebApp()
    return webapp.app


class TestPrivacyPolicyHandler:

    async def test_http_server_client(self, http_server_client):
        # http_server_client fetches from the `app` fixture and takes path
        resp = await http_server_client.fetch('/privacy-policy')
        assert resp.code == 200
        assert b"<title>Privacy Policy</title>" in resp.body


class TestTermsOfServiceHandler:

    async def test_http_server_client(self, http_server_client):
        # http_server_client fetches from the `app` fixture and takes path
        resp = await http_server_client.fetch('/terms-of-service')
        assert resp.code == 200
        assert b"<title>Terms of Service</title>" in resp.body


class TestHealthHandler:

    async def test_error(self, http_server_client):
        newspaper_assistent.MONGODB_DATABASE = None
        resp = await http_server_client.fetch('/health', raise_error=False)
        assert resp.code == 500

    async def test_read_timeout(self, http_server_client):
        newspaper_assistent.MONGODB_DATABASE = DEFAULT_CONNECTION_NAME
        newspaper_assistent.MONGODB_SERVER_SELECTION_TIMEOUT_MS = 100
        newspaper_assistent.initialize(reconnect=True)
        with pytest.raises(HTTPTimeoutError):
            await http_server_client.fetch('/health', request_timeout=0.001)

    async def test_ok(self, http_server_client, use_mongo_instance):
        resp = await http_server_client.fetch('/health', request_timeout=2, raise_error=False)
        assert 200 == resp.code
        assert b"OK" == resp.body

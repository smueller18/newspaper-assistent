#
# Copyright 2018 Stephan Müller
#
# Licensed under the MIT license

from threading import Thread

import tornado.httpserver
import tornado.ioloop
import tornado.netutil
import tornado.testing
import tornado.web

from newspaper_assistent.webapp import NewspaperAssistentHandler


class TestHealthHandler:
    port = 0

    @classmethod
    def setup_class(cls):
        # get random free port
        socket, cls.port = tornado.testing.bind_unused_port()
        socket.close()

        cls.app = tornado.web.Application([
            ("/newspaper-assistent", NewspaperAssistentHandler),
        ], autoreload=True)
        cls.app.listen(cls.port)
        cls.thread = Thread(name="tornado_newspaper-assistent_handler", target=tornado.ioloop.IOLoop.current().start)
        cls.thread.start()

    @classmethod
    def teardown_class(cls):
        tornado.ioloop.IOLoop.current().stop()

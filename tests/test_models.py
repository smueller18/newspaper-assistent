#
# Copyright 2018 Stephan Müller
#
# Licensed under the MIT license

import datetime

from newspaper_assistent.models import User, Article, UserSettings, UserArticleLifecycle
from tests import integration_test
from tests.mongo import MongoInstance


class TestUser:
    mongo_instance = MongoInstance()
    user = None

    @classmethod
    def setup_class(cls):
        cls.mongo_instance.run()
        cls.user = User(google_id="12345", name="Test", email="mail@example.org")
        cls.user.save()

    @classmethod
    def teardown_class(cls):
        del cls.mongo_instance

    @integration_test
    def test_update_section(self):
        assert len(User.objects) == 1

        self.user.update_section("Aus aller Welt")
        assert User.objects.first().settings.section == "Aus aller Welt"

    @integration_test
    def test_delete_section(self):
        self.user.delete_section()
        assert User.objects.first().settings.section is None

    @integration_test
    def test_reset_section(self):
        self.user.update_section("Aus aller Welt")
        assert User.objects.first().settings.section == "Aus aller Welt"
        self.user.reset_settings()
        assert User.objects.first().settings.section is None


class TestArticle:
    article = Article(
        newspaper="newspaper",
        locale="locale",
        date=datetime.date(2019, 1, 1),
        section='section',
        location='location',
        title='title',
        subtitle='subtitle',
        intro_text='intro_text',
        full_text='full_text',
        author='author',
        duration=1,
    )

    def test_has_intro(self):
        article = Article()
        assert not article.has_intro()

        assert self.article.has_intro()

    def test_headline(self):
        assert self.article.headline() == (
            'location.<break time="750ms"/>title. <break time="250ms"/>subtitle.<break time="750ms"/>'
            'Geschrieben von author.<break time="1s"/>Die geschätzte Vorlesedauer beträgt etwa'
            ' <say-as interpret-as="unit">1 second</say-as>.'
        )

        settings = UserSettings(include_author=False, include_duration_prediction=False)
        user = User(settings=settings)
        assert self.article.headline(user) == (
            'location.<break time="750ms"/>title. <break time="250ms"/>subtitle.'
        )

    def test_text(self):
        article = Article(full_text="full_text")
        assert article.text == "full_text"

        article.intro_text = "intro_text"
        assert article.text == 'intro_text<break time="1s"/>full_text'


class TestUserArticleLifecycle:
    mongo_instance = MongoInstance()

    @classmethod
    def setup_class(cls):
        cls.mongo_instance.run()

    @classmethod
    def teardown_class(cls):
        del cls.mongo_instance

    @integration_test
    def test_delete(self):
        user = User(google_id="12345", name="Test", email="mail@example.org")
        user.save()
        article_lifecycle = UserArticleLifecycle(user=user)
        article_lifecycle.save()
        assert len(UserArticleLifecycle.objects) == 1
        user.delete()
        assert len(UserArticleLifecycle.objects) == 0

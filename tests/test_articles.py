#
# Copyright 2018 Stephan Müller
#
# Licensed under the MIT license

import os

import yaml

from newspaper_assistent.articles import get_unread_articles, set_article_state, count_sections, list_sections, \
    list_sections_ssml
from newspaper_assistent.models import Article, ArticleState, User, UserArticleLifecycle, ArticleStateName
from tests import integration_test
from tests.mongo import MongoInstance

__dirname__ = os.path.abspath(os.path.dirname(__file__))


def insert_articles():
    with open(os.path.join(__dirname__, "resources", "bz-freiburg-2016-04-30.yml")) as resource:
        newspaper = yaml.load(resource)

    for article in newspaper['articles']:
        article_obj = Article(
            newspaper=newspaper.get('newspaper'),
            locale=newspaper.get('locale'),
            date=newspaper.get('date'),
            section=article.get('section'),
            location=article.get('location'),
            title=article.get('title'),
            subtitle=article.get('subtitle'),
            intro_text=article.get('intro_text'),
            full_text=article.get('full_text'),
            author=article.get('author'),
            duration=article.get('duration'),
        )
        article_obj.save()


class TestArticles:
    mongo_instance = MongoInstance()

    @classmethod
    def setup_class(cls):
        cls.mongo_instance.run()

        insert_articles()

        assert len(Article.objects) == 145

        cls.user = User(google_id="12345", name="Test", email="mail@example.org")
        cls.user.save()
        assert len(User.objects) == 1

        cls.article_reading = ArticleState(state=ArticleStateName.TITLE_READING.value)
        cls.article_read = ArticleState(state=ArticleStateName.TITLE_READ.value)
        user_article_lifecycle = UserArticleLifecycle(
            user=cls.user,
            article=Article.objects.first(),
            states=[cls.article_reading, cls.article_read]
        )
        user_article_lifecycle.save()
        assert len(UserArticleLifecycle.objects) == 1

    @classmethod
    def teardown_class(cls):
        del cls.mongo_instance

    @integration_test
    def test_get_unread_articles(self):
        article = get_unread_articles(self.user).first()
        assert isinstance(article, Article)

    @integration_test
    def test_set_article_state(self):
        set_article_state(self.user, Article.objects.first(), ArticleStateName.INTRO_READING)
        assert len(UserArticleLifecycle.objects.first().states) == 3

        set_article_state(self.user, Article.objects[1], ArticleStateName.INTRO_READING)
        assert UserArticleLifecycle.objects.count() == 2
        assert len(UserArticleLifecycle.objects[1].states) == 1

    @integration_test
    def test_count_sections(self):
        assert count_sections() == 18

    @integration_test
    def test_list_sections(self):
        expected = sorted([
            'Aus aller Welt',
            'Breisgau-Hochschwarzwald',
            'clever Leben',
            'Die dritte Seite',
            'Forum',
            'Freiburg',
            'Kommentar & Analyse',
            'Kultur',
            'Kultur und Medien',
            'Land und Region',
            'Politik Ausland',
            'Politik Inland',
            'Rund um Freiburg',
            'Sport',
            'Stadtteile',
            'Thema des Tages',
            'Titelblatt',
            'Wirtschaft'
        ])
        assert list_sections() == expected

    @integration_test
    def test_list_sections_ssml(self):
        assert list_sections_ssml().count(',<break time="1s"/>') == 18 - 1

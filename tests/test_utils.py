#
# Copyright 2018 Stephan Müller
#
# Licensed under the MIT license

import pytest
import os

from newspaper_assistent.utils import google_oath2_decode, trim, InvalidTokenError, time_duration_to_ssml


@pytest.mark.parametrize("expected,actual", [
    ("Hello", "  Hello  "),
    ("HelloWorld", "  Hello\tWorld  "),
    ("Hello World", "  Hello\t\n World  "),
    ("Hello World", "  Hello\t\n World  \t"),
    ("Hello World", "  Hello\t\n World  \t  "),
    ("Hello World", "  Hello\t\n World  \t  \n"),
])
def test_trim(expected, actual):
    assert expected == trim(actual)


def test_trim_nostring():
    assert 1 == trim(1)


@pytest.mark.skipif(os.getenv("TEST_GOOGLE_OAUTH2_TOKEN", "") == "",
                    reason="Environment variable TEST_GOOGLE_OAUTH2_TOKEN is not defined.")
def test_google_oath2_token_live():
    userid = google_oath2_decode(os.getenv("TEST_GOOGLE_OAUTH2_TOKEN"))
    assert userid['sub'].isdigit()


def test_google_oauth2_decode():
    with pytest.raises(InvalidTokenError) as e:
        google_oath2_decode("wrongtoken")
    assert str(e.value) == "Decoding token failed."


@pytest.mark.parametrize("duration,expected", [
    (20, '<say-as interpret-as="unit">20 second</say-as>'),
    (60, '<say-as interpret-as="unit">1 minute</say-as>'),
    (80, '<say-as interpret-as="unit">1 minute</say-as> und <say-as interpret-as="unit">20 second</say-as>'),
    (350, '<say-as interpret-as="unit">5 minute</say-as> und <say-as interpret-as="unit">50 second</say-as>'),
])
def test_time_duration_to_ssml(duration, expected):
    assert time_duration_to_ssml(duration) == expected

#
# Copyright 2018 Stephan Müller
#
# Licensed under the MIT license

from newspaper_assistent.dictionary import SerializableDict


def test_serializable_dict():
    dictionary = SerializableDict(dict())
    assert dictionary.serialize() == '{}'
    assert dictionary.encode() == b'{}'
